/*  Multithreading
 * 1) UI Thread
 * 2) Backgroud Thread <-- AsyncTsk
 * 
 * 
 * 
 * 
 * */

package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String province ="bangkok";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Do not allow the layout to be rotated based on the orientation of devices 
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String,String>>();
		File infile = getBaseContext().getFileStreamPath("weather.tsv");
		if (infile.exists()) {
			try {
				Scanner sc = new Scanner(infile);
				while(sc.hasNextLine()) {
					province = sc.nextLine();
					
				}
				sc.close();
			} catch (FileNotFoundException e) {
				//Do nothing
			}
		}
		
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		
		
		
		setListAdapter(adapter);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch(id) {
		case R.id.action_refresh:
			reload(3,province);
			return true;
		case R.id.action_settings:
			Toast t = Toast.makeText(this, 
					"setting is not available", 
					Toast.LENGTH_LONG);
			t.show();
			return true;
		case R.id.bangkok:
			province = "bangkok";
			reload(0,province);
			return true;
		case R.id.nontaburi:
			province = "nonthaburi";
			reload(0,province);
			return true;
		case R.id.pathumthani:
			province = "pathumthani";
			reload(0,province);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	public void reload(int T,String P)
	{
		try {
		//open file
		//private == file only use within this application only
		FileOutputStream outfile = openFileOutput("weather.tsv", MODE_PRIVATE);
		PrintWriter p = new PrintWriter(outfile);
		//data is separated by a tap
			p.write(P);
		
		p.flush(); 
		p.close();
		outfile.close();
	} catch (FileNotFoundException e) {
		Toast t = Toast.makeText(this, "Error: Unable to save data", 
				Toast.LENGTH_SHORT);
		t.show();
	} catch (IOException e) {
		Toast t = Toast.makeText(this, "Error: Unable to save data", 
				Toast.LENGTH_SHORT);
		t.show();
	}
		
		
		
		//check if the device is connected to a network
				ConnectivityManager mgr = (ConnectivityManager)
						getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo info = mgr.getActiveNetworkInfo();
				if (info != null && info.isConnected()) {
					//the device is connected to a network
					//Load data
					//Before loading, we compare the current time and the last update
					long current = System.currentTimeMillis();  //keep timestamp
					if (current - lastUpdate > T*60*1000) {
						//We start an AsyncTask for loading data
						WeatherTask task = new WeatherTask(this);
						task.execute("http://cholwich.org/"+P+".json");
						
					}
				}
				else {
					Toast t = Toast.makeText(this, 
							"No Internet Connectivity on this device. Check your setting", 
							Toast.LENGTH_LONG);
					t.show();
				}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		reload(5,province);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		
		@Override
		//executed under the UI thread before the task start
		protected void onPreExecute() {
			super.onPreExecute();
			
			
			
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		@Override
		//executed under the UI thread after the task start
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), 
					result, Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			//set the title of the activity
			setTitle(province+ " Weather");
		}

		//Executed under the background thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				//Get the first parameter string, and use it as a URL
				URL url = new URL(params[0]);
				//create a connection to the URL
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				//To read data from the webserver
				//if wanna write output to website  http.setDoOutput(true);
				http.setDoInput(true);
				http.connect();
				
				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					//Extract values from the obtained data
					//need to know format of the data first
					
					//temp_min, temp_max, wind speed, wind deg.
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Pressure");
					double pressure = jmain.getDouble("pressure");
					record.put("value", String.format(Locale.getDefault(), "%.1f mmhg", pressure));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "humidity");
					double humid = jmain.getDouble("humidity");
					record.put("value", String.format(Locale.getDefault(), "%.1f percent", humid));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Max Temperature");
					double temp_max = jmain.getDouble("temp_max")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius",temp_max));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Min Temperature");
					double temp_min = jmain.getDouble("temp_min")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp_min));
					list.add(record);
					
					//extract description
					//"weather" :[{"description": "....."}]
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String description  = w0.getString("description");
					record = new HashMap<String,String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);
					
				
					JSONObject jwind = json.getJSONObject("wind");
					record = new HashMap<String,String>();
					record.put("name", "wind speed");
					double windSpeed = jwind.getDouble("speed");
					record.put("value", String.format(Locale.getDefault(), "%.1f km/hr", windSpeed));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "wind Deg");
					double windDeg = jwind.getDouble("deg");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree", windDeg));
					list.add(record);
					
					
					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}

}

